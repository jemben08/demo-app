package com.demo.app.login;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class LoginViewModel extends ViewModel implements OnCompleteListener<AuthResult> {

    private MutableLiveData<Boolean> loading = new MutableLiveData<>();
    private MutableLiveData<AuthResult> success = new MutableLiveData<>();
    private MutableLiveData<Exception> error = new MutableLiveData<>();

    private FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();

    public void login(String email, String password) {
        loading.setValue(true);
        firebaseAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this);
    }

    @Override
    public void onComplete(@NonNull Task<AuthResult> task) {
        loading.setValue(false);
        if (task.isSuccessful()) {
            success.setValue(task.getResult());
        } else {
            error.setValue(task.getException());
        }
    }

    public LiveData<Boolean> getLoading() {
        return loading;
    }

    public LiveData<AuthResult> getSuccess() {
        return success;
    }

    public LiveData<Exception> getError() {
        return error;
    }
}
