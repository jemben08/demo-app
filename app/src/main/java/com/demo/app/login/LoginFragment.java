package com.demo.app.login;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.ViewModelProvider;

import com.demo.app.databinding.FragmentLoginBinding;
import com.demo.app.dialog.SimpleMessageDialogFragment;
import com.demo.app.landing.LandingCallback;
import com.demo.app.dialog.LoadingDialogFragment;
import com.google.firebase.auth.AuthResult;

public class LoginFragment extends Fragment
        implements View.OnClickListener {

    private FragmentLoginBinding binding;
    private LoginViewModel viewModel;
    private LandingCallback callback;

    public static LoginFragment newInstance() {
        return new LoginFragment();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        callback = (LandingCallback) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ViewModelProvider provider = new ViewModelProvider(this);
        viewModel = provider.get(LoginViewModel.class);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        binding = FragmentLoginBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view,
                              @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewModel.getLoading().observe(
                getViewLifecycleOwner(),
                loading -> {
                    if (loading) {
                        showLoading();
                    } else {
                        hideLoading();
                    }
                }
        );
        viewModel.getError().observe(
                getViewLifecycleOwner(),
                e -> showErrorMessage(e.getMessage())
        );
        viewModel.getSuccess().observe(
                getViewLifecycleOwner(),
                result -> navToMainActivity(result)
        );
        binding.btnLogin.setOnClickListener(this);
    }

    private void navToMainActivity(AuthResult result) {
        callback.navToMainActivity();
    }

    private void showErrorMessage(String message) {
        FragmentManager fm = requireActivity()
                .getSupportFragmentManager();
        SimpleMessageDialogFragment.newInstance(null, message)
                .show(fm, SimpleMessageDialogFragment.TAG);
    }

    private void hideLoading() {
        FragmentManager fm = requireActivity()
                .getSupportFragmentManager();
        LoadingDialogFragment.hide(fm);
    }

    private void showLoading() {
        FragmentManager fm = requireActivity()
                .getSupportFragmentManager();
        LoadingDialogFragment.show(fm);
    }

    @Override
    public void onClick(View v) {
        final String email = binding.edtEmail.getText().toString();
        final String password = binding.edtPassword.getText().toString();
        viewModel.login(email, password);
    }
}
