package com.demo.app.db;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.demo.app.task.Task;
import com.demo.app.task.TaskDao;

@Database(entities = {Task.class}, version = 1,
        exportSchema = false)
public abstract class AppDb extends RoomDatabase {

    public abstract TaskDao taskDao();

}
