package com.demo.app;

import android.os.Bundle;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.demo.app.main.MainFragment;
import com.demo.app.task.TasksFragment;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.google.firebase.messaging.FirebaseMessaging;

public class MainActivity extends AppCompatActivity
        implements MainFragment.MainCallback, OnCompleteListener<String> {

    public static final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState == null) {
            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
            String email = user.getEmail();
            FirebaseCrashlytics.getInstance().setCustomKey("email", email);

            getSupportFragmentManager().beginTransaction()
                    .add(R.id.main_container, TasksFragment.newInstance())
                    .commit();
        }

    }

    @Override
    protected void onStart() {
        super.onStart();
        FirebaseMessaging.getInstance()
                .getToken()
                .addOnCompleteListener(this);
    }

    @Override
    public void navToAccountFragment() {

    }

    @Override
    public void onComplete(@NonNull Task<String> task) {
        if (task.isSuccessful()) {
            String token = task.getResult();
            Log.v(TAG, "FirebaseToken : " + token);
        }
    }
}