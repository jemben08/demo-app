package com.demo.app.task;

import android.app.Application;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.DividerItemDecoration;

import com.demo.app.databinding.FragmentTasksBinding;

public class TasksFragment extends Fragment implements View.OnClickListener, TaskViewHolder.OnDeleteListener {

    private FragmentTasksBinding binding;
    private TasksAdapter adapter;
    private TaskViewModel viewModel;

    public static Fragment newInstance() {
        return new TasksFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Application application = (Application) requireContext()
                .getApplicationContext();
        ViewModelProvider.AndroidViewModelFactory factory = ViewModelProvider
                .AndroidViewModelFactory.getInstance(application);
        ViewModelProvider provider = new ViewModelProvider(requireActivity(), factory);
        viewModel = provider.get(TaskViewModel.class);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        binding = FragmentTasksBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view,
                              @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        adapter = new TasksAdapter(this);
        binding.recycler.setAdapter(adapter);
        binding.recycler.addItemDecoration(new DividerItemDecoration(requireContext(), DividerItemDecoration.VERTICAL));
        binding.btnAdd.setOnClickListener(this);
        viewModel.getTasks().observe(this,
                tasks -> adapter.setUpdates(tasks));
    }

    @Override
    public void onClick(View v) {
        TaskFormFragment.newInstance()
                .show(getFragmentManager(), TaskFormFragment.TAG);
    }

    @Override
    public void delete(Task task) {
        viewModel.delete(task);
    }
}
