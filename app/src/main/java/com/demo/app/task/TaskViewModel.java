package com.demo.app.task;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.room.Room;

import com.demo.app.R;
import com.demo.app.db.AppDb;
import com.demo.app.task.network.TaskApi;
import com.demo.app.task.network.TaskRepository;

import java.util.List;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class TaskViewModel extends AndroidViewModel {
    public static final String TAG = "TaskViewModel";
    private CompositeDisposable disposables = new CompositeDisposable();
    private final AppDb db;
    private TaskRepository taskRepository;
    private MutableLiveData<List<Task>> mTasks = new MutableLiveData<>();

    public TaskViewModel(@NonNull Application application) {
        super(application);
        db = Room.databaseBuilder(getApplication().getApplicationContext(),
                AppDb.class, "app-db").build();

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        TaskApi userApi = new Retrofit.Builder()
                .baseUrl("https://fir-app-b1707-default-rtdb.firebaseio.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(new OkHttpClient.Builder()
                        .addInterceptor(interceptor)
                        .build())
                .build()
                .create(TaskApi.class);

        taskRepository = new TaskRepository(userApi, db);
        fetch();
    }

    private void fetch() {
        disposables.add(taskRepository.getTask().subscribeOn(Schedulers.io())
                .subscribe(tasks -> {
                            mTasks.postValue(tasks);
                        },
                        throwable -> {
                            Log.e(TAG, throwable.getMessage(), throwable.getCause());
                        }));
    }

    public void addTask(Task task) {
        task.setTaskId(mTasks.getValue().size());
        disposables.add(taskRepository.saveTask(task)
                .subscribeOn(Schedulers.io())
                .subscribe(() -> {
                    Log.i(TAG, getApplication().getString(R.string.log_insert));
                    fetch();
                }, throwable -> {
                    Log.i(TAG, throwable.getMessage(),
                            throwable.getCause());
                }));
    }

    public LiveData<List<Task>> getTasks() {
        return mTasks;
    }

    public void delete(Task task) {
        disposables.add(db.taskDao().delete(task).subscribeOn(Schedulers.io())
                .subscribe(() -> {
                    Log.i(TAG, "Delete!");
                }, throwable -> {
                    Log.i(TAG, throwable.getMessage(), throwable.getCause());
                }));
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        disposables.dispose();
        disposables.clear();
    }
}
