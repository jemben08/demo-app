package com.demo.app.task;

import androidx.recyclerview.widget.RecyclerView;

import com.demo.app.databinding.ItemTaskBinding;

class TaskViewHolder extends RecyclerView.ViewHolder {

    private final ItemTaskBinding binding;
    private final OnDeleteListener listener;

    public TaskViewHolder(ItemTaskBinding binding, OnDeleteListener listener) {
        super(binding.getRoot());
        this.binding = binding;
        this.listener = listener;
    }

    public void bind(Task task) {
        binding.txvTask.setText(task.getTaskId() +
                " - " + task.getName());
        binding.txvDesc.setText(task.getDesc());
        binding.btnDelete.setOnClickListener(v -> listener.delete(task));
    }

    interface OnDeleteListener {
        void delete(Task task);
    }

}