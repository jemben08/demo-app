package com.demo.app.task.network;

import com.demo.app.task.network.model.TaskRemote;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Maybe;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.PATCH;
import retrofit2.http.Path;

public interface TaskApi {

    @GET("tasks/{userId}.json")
    Maybe<List<TaskRemote>> getTask(@Path("userId") String userId);

    @PATCH("tasks/{userId}/{taskId}.json")
    Completable saveTask(@Path("userId") String userId,
                         @Path("taskId") int taskId,
                         @Body TaskRemote request);

    @DELETE("tasks/{userId}/{taskId}.json")
    Maybe<List<TaskRemote>> getTask(@Path("userId") String userId,
                                    @Path("taskId") int taskId);

}
