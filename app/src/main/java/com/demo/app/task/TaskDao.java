package com.demo.app.task;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

import io.reactivex.Completable;

@Dao
public interface TaskDao {

    @Query("SELECT * from tasks")
    LiveData<List<Task>> getAll();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Completable insert(Task task);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(List<Task> task);

    @Delete
    Completable delete(Task task);

}
