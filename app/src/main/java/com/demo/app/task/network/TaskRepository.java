package com.demo.app.task.network;

import com.demo.app.db.AppDb;
import com.demo.app.task.Task;
import com.demo.app.task.network.model.TaskRemote;
import com.google.firebase.auth.FirebaseAuth;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Maybe;

public class TaskRepository {
    private final TaskApi taskApi;
    private final AppDb appDb;

    public TaskRepository(TaskApi taskApi, AppDb appDb) {
        this.taskApi = taskApi;
        this.appDb = appDb;
    }

    public Maybe<List<Task>> getTask() {
        String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
        return taskApi.getTask(uid).map(list -> {
            List<Task> tasks = new ArrayList<>();
            for (int i = 0; i < list.size(); i++) {
                TaskRemote model = list.get(i);
                Task task = new Task();
                task.setTaskId(i);
                task.setName(model.getName());
                task.setDesc(model.getDesc());
                tasks.add(task);
            }
            appDb.taskDao().insert(tasks);
            return tasks;
        });
    }

    public Completable saveTask(Task task) {
        String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
        TaskRemote request = new TaskRemote();
        request.setName(task.getName());
        request.setDesc(task.getDesc());
        return appDb.taskDao()
                .insert(task)
                .andThen(taskApi.saveTask(uid, task.getTaskId(), request));
    }

}
