package com.demo.app.task;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.demo.app.databinding.ItemTaskBinding;

import java.util.ArrayList;
import java.util.List;

public class TasksAdapter extends RecyclerView.Adapter<TaskViewHolder> {

    private final List<Task> items = new ArrayList<>();
    private final TaskViewHolder.OnDeleteListener listener;

    public TasksAdapter(TaskViewHolder.OnDeleteListener listener) {
        this.listener = listener;
    }

    public void setUpdates(List<Task> updates) {
        items.clear();
        items.addAll(updates);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public TaskViewHolder onCreateViewHolder(@NonNull ViewGroup parent,
                                             int viewType) {
        ItemTaskBinding binding = ItemTaskBinding
                .inflate(LayoutInflater.from(parent.getContext()),
                        parent, false);
        return new TaskViewHolder(binding, listener);
    }

    @Override
    public void onBindViewHolder(@NonNull TaskViewHolder holder, int position) {
        holder.bind(items.get(position));
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

}
