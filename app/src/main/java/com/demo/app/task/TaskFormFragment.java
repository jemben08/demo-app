package com.demo.app.task;

import android.app.Application;
import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModelProvider;

import com.demo.app.databinding.FragmentTaskFormBinding;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;

public class TaskFormFragment extends DialogFragment
        implements View.OnClickListener {
    public static String TAG = "TaskFormFragment";
    private FragmentTaskFormBinding binding;
    private TaskViewModel viewModel;

    public static DialogFragment newInstance() {
        return new TaskFormFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Application application = (Application)
                requireContext().getApplicationContext();
        ViewModelProvider.AndroidViewModelFactory factory =
                ViewModelProvider.AndroidViewModelFactory
                        .getInstance(application);
        ViewModelProvider provider =
                new ViewModelProvider(requireActivity(),
                        factory);
        viewModel = provider.get(TaskViewModel.class);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        super.onCreateDialog(savedInstanceState);
        LayoutInflater inflater = LayoutInflater.from(
                requireContext());
        binding = FragmentTaskFormBinding.inflate(
                inflater, null, false);

        AlertDialog alertDialog =
                new MaterialAlertDialogBuilder(requireContext())
                .setTitle("New Task")
                .setView(binding.getRoot())
                .setCancelable(false)
                .setNegativeButton("Cancelar", null)
                .setPositiveButton("Agregar", null)
                .create();
        alertDialog.setCancelable(false);
        alertDialog.setCanceledOnTouchOutside(false);
        return alertDialog;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    @Override
    public void onStart() {
        super.onStart();
        final Dialog dialog = getDialog();
        if (dialog instanceof AlertDialog) {
            final AlertDialog alertDialog = (AlertDialog) dialog;
            alertDialog.getButton(AlertDialog.BUTTON_POSITIVE)
                    .setOnClickListener(this);
        }
    }

    @Override
    public void onClick(View v) {
        Task task = new Task();
        task.setName(binding.edtName.getText().toString());
        task.setDesc(binding.edtDesc.getText().toString());
        viewModel.addTask(task);
        dismiss();
    }

}
