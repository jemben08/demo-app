package com.demo.app.dialog;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;

import com.demo.app.R;

public class LoadingDialogFragment extends DialogFragment {

    private static final String TAG = "LoadingDialogFragment";

    public static void show(final FragmentManager fm) {
        DialogFragment fragment = (DialogFragment)
                fm.findFragmentByTag(TAG);
        if (fragment == null) {
            fragment = new LoadingDialogFragment();
        }
        fragment.show(fm, TAG);
    }

    public static void hide(final FragmentManager fm) {
        DialogFragment fragment = (DialogFragment) fm
                .findFragmentByTag(TAG);
        if (fragment != null) {
            fragment.dismiss();
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        return inflater.inflate(R.layout.fragment_loading,
                container, false);
    }

}
