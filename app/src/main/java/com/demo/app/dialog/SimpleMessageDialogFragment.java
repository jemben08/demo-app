package com.demo.app.dialog;

import android.app.Dialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.demo.app.R;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;

public class SimpleMessageDialogFragment extends DialogFragment {

    public static final String TAG = "SimpleMessageDialogFragment";
    private static final String ARG_TITLE = "TITLE";
    private static final String ARG_MESSAGE = "MESSAGE";

    public static DialogFragment newInstance(String title,
                                             String message) {
        SimpleMessageDialogFragment dialogFragment =
                new SimpleMessageDialogFragment();
        Bundle args = new Bundle();
        args.putString(ARG_TITLE, title);
        args.putString(ARG_MESSAGE, message);
        dialogFragment.setArguments(args);
        return dialogFragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        super.onCreateDialog(savedInstanceState);
        final Bundle args = getArguments();
        String title = args.getString(ARG_TITLE,
                getString(R.string.title_simple_message_default)
        );
        String message = args.getString(ARG_MESSAGE);
        return new MaterialAlertDialogBuilder(requireContext())
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(R.string.btn_accept, null)
                .create();
    }
}
