package com.demo.app.landing;

import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseUser;

public interface LandingCallback {

    void navToLoginFragment();

    void navToSignUpFragment();

    void navToMainActivity();

}