package com.demo.app.landing;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.demo.app.MainActivity;
import com.demo.app.R;
import com.demo.app.databinding.ActivityLandingBinding;
import com.demo.app.login.LoginFragment;
import com.demo.app.signup.SignUpFragment;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class LandingActivity extends AppCompatActivity
        implements LandingCallback {

    private ActivityLandingBinding binding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityLandingBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        if (savedInstanceState == null) {

            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
            if (user != null) {
                navToMainActivity();
                finish();
                return;
            }
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.main_container, LandingFragment.newInstance())
                    .commit();
        }
    }

    @Override
    public void navToLoginFragment() {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.main_container, LoginFragment.newInstance())
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void navToSignUpFragment() {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.main_container, SignUpFragment.newInstance())
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void navToMainActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}
