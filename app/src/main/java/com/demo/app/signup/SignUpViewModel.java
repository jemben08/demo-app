package com.demo.app.signup;

import android.util.Patterns;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class SignUpViewModel extends ViewModel implements OnCompleteListener<AuthResult> {

    private static final int MIN_LENGTH = 8;

    private MutableLiveData<String> emailError = new MutableLiveData<>();
    private MutableLiveData<String> passwordError = new MutableLiveData<>();
    private MutableLiveData<String> passwordConfirmError = new MutableLiveData<>();
    private MutableLiveData<Boolean> loading = new MutableLiveData<>();
    private MutableLiveData<AuthResult> success = new MutableLiveData<>();
    private MutableLiveData<Exception> error = new MutableLiveData<>();
    private FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();

    public void signUp(String email, String password,
                       String confirmPassword) {
        if (isValidForm(email, password, confirmPassword)) {
            loading.setValue(true);
            firebaseAuth.createUserWithEmailAndPassword(email, password)
                    .addOnCompleteListener(this);
        }
    }

    private boolean isValidForm(String email, String password,
                                String confirmPassword) {
        emailError.setValue(null);
        passwordError.setValue(null);
        passwordConfirmError.setValue(null);

        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            emailError.setValue("El correo electrónico no es valido");
            return false;
        }

        if (password.length() < MIN_LENGTH) {
            passwordError.setValue("Debe ingresar por lo menos " + MIN_LENGTH + " caracteres");
            return false;
        }

        if (!password.equals(confirmPassword)) {
            passwordConfirmError.setValue("Las contraseñas no coinciden");
            return false;
        }

        return true;
    }

    @Override
    public void onComplete(@NonNull Task<AuthResult> task) {
        loading.setValue(false);
        if (task.isSuccessful()) {
            success.setValue(task.getResult());
        } else {
            error.setValue(task.getException());
        }
    }

    public LiveData<String> getEmailError() {
        return emailError;
    }

    public LiveData<String> getPasswordError() {
        return passwordError;
    }

    public LiveData<String> getPasswordConfirmError() {
        return passwordConfirmError;
    }

    public LiveData<Boolean> getLoading() {
        return loading;
    }

    public LiveData<AuthResult> getSuccess() {
        return success;
    }

    public LiveData<Exception> getError() {
        return error;
    }
}
