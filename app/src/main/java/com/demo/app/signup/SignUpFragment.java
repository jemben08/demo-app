package com.demo.app.signup;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.ViewModelProvider;

import com.demo.app.databinding.FragmentSignUpBinding;
import com.demo.app.dialog.SimpleMessageDialogFragment;
import com.demo.app.landing.LandingCallback;
import com.demo.app.dialog.LoadingDialogFragment;
import com.google.firebase.auth.AuthResult;

public class SignUpFragment extends Fragment implements View.OnClickListener {

    private FragmentSignUpBinding binding;
    private SignUpViewModel viewModel;
    private LandingCallback callback;

    public static SignUpFragment newInstance() {
        return new SignUpFragment();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        callback = (LandingCallback) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ViewModelProvider provider = new ViewModelProvider(this);
        viewModel = provider.get(SignUpViewModel.class);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        binding = FragmentSignUpBinding.inflate(inflater,
                container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view,
                              @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewModel.getEmailError().observe(
                getViewLifecycleOwner(),
                s -> binding.tilEmail.setError(s)
        );
        viewModel.getPasswordError().observe(
                getViewLifecycleOwner(),
                s -> binding.tilPassword.setError(s)
        );
        viewModel.getPasswordConfirmError().observe(
                getViewLifecycleOwner(),
                s -> binding.tilConfirmPassword.setError(s)
        );
        viewModel.getLoading().observe(
                getViewLifecycleOwner(),
                loading -> {
                    if (loading) {
                        showLoading();
                    } else {
                        hideLoading();
                    }
                }
        );
        viewModel.getError().observe(
                getViewLifecycleOwner(),
                error -> showErrorMessage(error.getMessage())
        );
        viewModel.getSuccess().observe(
                getViewLifecycleOwner(),
                authResult -> navToMainActivity(authResult));
        binding.btnSignUp.setOnClickListener(this);
    }

    private void showLoading() {
        FragmentManager fm = requireActivity()
                .getSupportFragmentManager();
        LoadingDialogFragment.show(fm);
    }

    private void hideLoading() {
        FragmentManager fm = requireActivity()
                .getSupportFragmentManager();
        LoadingDialogFragment.hide(fm);
    }

    private void showErrorMessage(String message) {
        SimpleMessageDialogFragment.newInstance(
                null,
                message
        ).show(requireFragmentManager(), SimpleMessageDialogFragment.TAG);
    }

    private void navToMainActivity(AuthResult result) {
        callback.navToMainActivity();
    }

    @Override
    public void onClick(View v) {
        String email = binding.edtEmail.getText().toString();
        String password = binding.edtPassword.getText().toString();
        String confirmPassword = binding.edtConfirmPasswrod.getText().toString();
        viewModel.signUp(email, password, confirmPassword);
    }
}
