package com.demo.app.service;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.demo.app.App;
import com.demo.app.MainActivity;
import com.demo.app.R;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    @Override
    public void onNewToken(@NonNull String s) {
        super.onNewToken(s);

    }

    @Override
    public void onMessageReceived(@NonNull RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        Log.i("MyFirebaseMessaging", "onMessageReceived");

        Intent intent = new Intent(requireContext(), MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(requireContext(), 0, intent, 0);

        long[] pattern = {0, 100, 1000, 300, 200, 100, 500, 200, 100};

        RemoteMessage.Notification content = remoteMessage.getNotification();
        String title = content != null ? content.getTitle() : remoteMessage.getData().get("title");
        String body = content != null ? content.getBody() : remoteMessage.getData().get("body");
        Notification notification = new NotificationCompat
                .Builder(requireContext(), getString(R.string.default_notification_channel_id))
                .setContentTitle(title)
                .setContentText(body)
                .setVibrate(pattern)
                .setSmallIcon(R.drawable.ic_notify)
                .setContentIntent(pendingIntent)
                .addAction(R.drawable.ic_baseline_check_24, "Action", pendingIntent)
                .setAutoCancel(true)
                .build();

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(requireContext());
        notificationManager.notify(1, notification);

    }

    private Context requireContext() {
        return this;
    }

}
